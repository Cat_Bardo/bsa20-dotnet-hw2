﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.
using System.Collections.Generic;
using System.Net;

namespace CoolParking.BL.Models
{
    public static class Settings 
    {
        public readonly static decimal balance = 0;
        public readonly static int capacity = 10;
        public readonly static int rentTime = 5000;
        public readonly static int logTime = 60000;
        public readonly static Dictionary<VehicleType, decimal> rate = new Dictionary<VehicleType, decimal>
        {
            [VehicleType.PassengerCar] = 2,
            [VehicleType.Truck] = 5,
            [VehicleType.Bus] = 3.5M,
            [VehicleType.Motorcycle] = 1,
        };
        public readonly static decimal taxKoef = 2.5M;

    }
}