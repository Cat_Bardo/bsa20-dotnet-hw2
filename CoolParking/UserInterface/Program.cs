﻿using System;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using SimpleCMenu.Menu;
using System.Drawing;
using Console = Colorful.Console;
using System.IO;

namespace UserInterface
{
    class Program
    { 
        static ParkingService parkingService = new ParkingService(new TimerService(), new TimerService(), new LogService("Transaction.log"));
        static void Main(string[] args)
        {
            Console.SetWindowSize(72, 30);
            //title word
            string headerText = "Cool Parking";
            Console.Clear();

            // Setup the menu
            ConsoleMenu mainMenu = new ConsoleMenu();
            // Transaction menu
             ConsoleMenu subMenu0 = new ConsoleMenu();
            subMenu0.SubTitle = "---------------------------- Меню Транзакцiй -----------------------------";
            subMenu0.addMenuItem(0, "Поточнi транзакцiї", CurrentTransactions);
            subMenu0.addMenuItem(1, "Iсторiя транзакцiй", TransactionHistory);
            subMenu0.addMenuItem(2, "Вихiд", subMenu0.hideMenu);
            subMenu0.ParentMenu = mainMenu;
            //Vehicle menu
            ConsoleMenu subMenu1 = new ConsoleMenu();
            subMenu1.SubTitle = "---------------------------- Меню Транспорту -----------------------------";
            subMenu1.addMenuItem(0, "Список транспорту на паркiнгу", VehicleList);
            subMenu1.addMenuItem(1, "Поставити транспорт", AddVehicle);
            subMenu1.addMenuItem(2, "Забрати транспорт", RemoveVehicle);
            subMenu1.addMenuItem(3, "Поповнити транспортний засiб", TopUpVehicle);
            subMenu1.addMenuItem(4, "Вихiд", subMenu1.hideMenu);
            subMenu1.ParentMenu = mainMenu;
            // Cool Parking title for all menues
            mainMenu.Header = headerText;
            subMenu0.Header = mainMenu.Header;
            subMenu1.Header = mainMenu.Header;

            // Main menu
            mainMenu.SubTitle = "-------------------------------- Menu ----------------------------------";
            mainMenu.addMenuItem(0, "Поточний баланс", Balance) ;
            mainMenu.addMenuItem(1, "Заробленi кошти", Money);
            mainMenu.addMenuItem(2, "Вiльнi мiсця", FreePlaces);
            mainMenu.addMenuItem(3, "Транзакцiї", subMenu0.showMenu);
            mainMenu.addMenuItem(4, "Транспорт", subMenu1.showMenu);
            mainMenu.addMenuItem(5, "Вихiд", Exit);
            // Display the menu
            mainMenu.showMenu();
        }

        //Methods
        public static void  Balance()
        {
            Console.WriteLine("Для повернення у МЕНЮ натиснiть будь-яку клавiшу\n", Color.Khaki);
            Console.WriteLine($"Поточний баланс:{parkingService.GetBalance()}");
            Console.ReadKey();
        }

        public static void Money() 
        {
            Console.WriteLine("Для повернення у МЕНЮ натиснiть будь-яку клавiшу\n", Color.Khaki);
            decimal Sum = 0;
            foreach (var tr in parkingService.GetLastParkingTransactions()) 
            {
               Sum += tr.Sum;
            }
            Console.WriteLine($"Заробленi кошти:{Sum}");
           
            Console.ReadKey();

        }

        public static void FreePlaces()
        {
            Console.WriteLine("Для повернення у МЕНЮ натиснiть будь-яку клавiшу\n", Color.Khaki);
            Console.WriteLine($"Вiльнi мiсця:{parkingService.GetFreePlaces()}");
            Console.ReadKey();
        }

        public static void CurrentTransactions()
        {
            Console.WriteLine("Для повернення у МЕНЮ натичнiть будь-яку клавiшу\n", Color.Khaki);
            foreach (var tr in parkingService.GetLastParkingTransactions())
            {
                Console.WriteLine($"Id транспорту:{tr.Id} Сума:{tr.Sum} Час:{tr.Time}");
            }
            Console.ReadKey();
        }

        public static void TransactionHistory()
        {
            try
            {
                Console.WriteLine("Для повернення у МЕНЮ натиснiть будь-яку клавiшу\n", Color.Khaki);
                Console.WriteLine(parkingService.ReadFromLog());
                Console.ReadKey();
            }
            catch(InvalidOperationException) 
            {
                Console.WriteLine("Записи вiдсутнi", Color.Red);
                Console.ReadKey();
            }
        }

        public static void VehicleList()
        {
            Console.WriteLine("Для повернення у МЕНЮ натиснiть будь-яку клавiшу\n", Color.Khaki);
            if (parkingService.GetVehicles().Count == 0)
            {
                Console.WriteLine("Немає транспорту на парковцi", Color.Red);
            }
            else
            {
                foreach (var vehicle in parkingService.GetVehicles())
                {
                    Console.WriteLine($"Id транспорту:{vehicle.Id} Тип транспорту:{vehicle.VehicleType} Баланс:{vehicle.Balance}");
                }
            }
            
            Console.ReadKey();
        }

        public static void AddVehicle()
        {
            try
            {
                Console.SetWindowSize(72, 30);
                Console.WriteLine("Введiть Id:");
                string id = Console.ReadLine();
                Console.WriteLine("Введiть тип транспорту:\n0-легковий автомобiль,1-вантажiвка,2-автобус,3-мотоцикл");
                int type = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Введiть баланс:");
                decimal balance = Convert.ToDecimal(Console.ReadLine());
                parkingService.AddVehicle(new Vehicle(id, (VehicleType)type, balance));
                Console.WriteLine("Операцiя виконана успiшно!\n",Color.Green);
                Console.WriteLine("Для повернення у МЕНЮ натиснiть будь-яку клавiшу\n", Color.Khaki);
                Console.ReadKey();
            }
            catch (ArgumentException ex)
            {
                
                Console.WriteLine(ex.Message, Color.Red);
                Console.WriteLine("Будь-ласка повторiть спробу!\n");
                AddVehicle();
            }
            catch (InvalidOperationException) 
            { 
                Console.WriteLine("\nПомилка! Немає вільних місць\n", Color.Red);
                Console.WriteLine("Для повернення у меню нажмiть будь-яку клавiшу\n", Color.Khaki);
                Console.ReadKey();
            }
        }

        public static void RemoveVehicle() 
        {
            try
            {
                Console.SetWindowSize(72, 30);
                Console.WriteLine("Для повернення у меню нажмiть будь-яку клавiшу\n", Color.Khaki);
                Console.WriteLine("Введiть ID транспорту");
                parkingService.RemoveVehicle(Console.ReadLine());
                Console.WriteLine("Операція виконана успiшно!", Color.Green);
                Console.ReadKey();
            }
            catch (ArgumentException)
            {
                Console.WriteLine("\nНе правильно введений Id. Будь-ласка повторiть спробу!\n",Color.Red);
                RemoveVehicle();
            }
            catch (InvalidOperationException)
            {
                Console.WriteLine("\nПомилка! Баланс транспорту від'ємний. Поповніть баланс!\n",Color.Red);
                Console.WriteLine("Для повернення у МЕНЮ натиснiть будь-яку клавiшу\n", Color.Khaki);
                Console.ReadKey();
            }
        }

        public static void TopUpVehicle()
        {
            try
            {
                Console.SetWindowSize(72, 30);
                Console.WriteLine("Введiть Id:");
                string id = Console.ReadLine();
                Console.WriteLine("Введiть суму поповнення:");
                decimal sum = Convert.ToDecimal(Console.ReadLine());
                parkingService.TopUpVehicle(id,sum);
                Console.WriteLine("Операція виконана успiшно!", Color.Green);
                Console.ReadKey();
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine(ex.Message, Color.Red);
                Console.WriteLine("Будь-ласка повторiть спробу!\n");
                TopUpVehicle();
            }

        }
        public static void Exit()
        {
            Environment.Exit(0);
        }
    }
}
 